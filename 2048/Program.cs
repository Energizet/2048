﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2048
{
    class Program
    {

        static Random rand = new Random();
        static bool equal = false;
        static long scoup = 0;

        static void Main(string[] args)
        {
            Console.WriteLine("введите размер матрицы");
            int size = Convert.ToInt32(Console.ReadLine());

            int[][] arr = new int[size][];
            for (int i = 0; i < size; i++)
            {
                arr[i] = new int[size];
            }
            print(arr);
            print(arr);
            while (true)
            {
                switch (Console.ReadKey().Key)
                {
                    case ConsoleKey.A:
                    case ConsoleKey.LeftArrow:
                        arr = left(arr);
                        print(arr);
                        break;
                    case ConsoleKey.W:
                    case ConsoleKey.UpArrow:
                        arr = up(arr);
                        print(arr);
                        break;
                    case ConsoleKey.D:
                    case ConsoleKey.RightArrow:
                        arr = right(arr);
                        print(arr);
                        break;
                    case ConsoleKey.S:
                    case ConsoleKey.DownArrow:
                        arr = down(arr);
                        print(arr);
                        break;
                }
            }
        }

        static int[][] left(int[][] oldArr)
        {
            //Console.WriteLine("swipe left");
            int[][] arr = copy(oldArr);
            arr = swipe(arr);
            return arr;
        }

        static int[][] right(int[][] oldArr)
        {
            //Console.WriteLine("swipe right");
            int[][] arr = copy(oldArr);
            arr = revers(arr);
            arr = swipe(arr);
            arr = revers(arr);
            return arr;
        }

        static int[][] up(int[][] oldArr)
        {
            //Console.WriteLine("swipe up");
            int[][] arr = copy(oldArr);
            arr = tranparent(arr);
            arr = swipe(arr);
            arr = tranparent(arr);
            return arr;
        }

        static int[][] down(int[][] oldArr)
        {
            //Console.WriteLine("swipe down");
            int[][] arr = copy(oldArr);
            arr = tranparent(arr);
            arr = revers(arr);
            arr = swipe(arr);
            arr = revers(arr);
            arr = tranparent(arr);
            return arr;
        }

        static int[][] copy(int[][] arr)
        {
            List<int[]> newArr = new List<int[]>();
            foreach (var item in arr)
            {
                List<int> inner = new List<int>();
                foreach (var item2 in item)
                {
                    inner.Add(item2);
                }
                newArr.Add(inner.ToArray());
            }
            return newArr.ToArray();
        }

        static int[][] revers(int[][] arr)
        {
            List<int[]> newArr = new List<int[]>();
            foreach (var item in arr)
            {
                List<int> inner = new List<int>();
                for (int i = item.Length - 1; i >= 0; i--)
                {
                    inner.Add(item[i]);
                }
                newArr.Add(inner.ToArray());
            }
            return newArr.ToArray();
        }

        static int[][] tranparent(int[][] arr)
        {
            List<int[]> newArr = new List<int[]>();
            if (arr.Length > 0)
            {
                for (int i = 0; i < arr[0].Length; i++)
                {
                    List<int> inner = new List<int>();
                    for (int j = 0; j < arr.Length; j++)
                    {
                        inner.Add(arr[j][i]);
                    }
                    newArr.Add(inner.ToArray());
                }
            }
            return newArr.ToArray();
        }

        static int[][] swipe(int[][] oldArr)
        {
            int[][] arr = copy(oldArr);
            for (int i = 0; i < arr.Length; i++)
            {
                List<int> stek = new List<int>();
                for (int j = 0; j < arr[i].Length; j++)
                {
                    if (arr[i][j] != 0)
                    {
                        stek.Add(arr[i][j]);
                    }
                }
                List<int> newArr = new List<int>();
                while (stek.Count > 0)
                {
                    if (stek.Count > 1 && stek[0] == stek[1])
                    {
                        int s = stek[0] * 2;
                        newArr.Add(s);
                        scoup += s;
                        stek.RemoveAt(0);
                        stek.RemoveAt(0);
                    }
                    else
                    {
                        newArr.Add(stek[0]);
                        stek.RemoveAt(0);
                    }
                }
                int size = newArr.Count;
                for (int j = 0; j < arr[i].Length - size; j++)
                {
                    newArr.Add(0);
                }
                arr[i] = newArr.ToArray();
            }
            equal = true;
            for (int i = 0; i < arr.Length && equal; i++)
            {
                for (int j = 0; j < arr[i].Length && equal; j++)
                {
                    if (arr[i][j] != oldArr[i][j])
                    {
                        equal = false;
                    }
                }
            }
            return arr;
        }

        static void print(int[][] arr)
        {
            if (!equal)
            {
                List<int> xs = new List<int>();
                List<int> ys = new List<int>();
                for (int y = 0; y < arr.Length; y++)
                {
                    for (int x = 0; x < arr[y].Length; x++)
                    {
                        if (arr[y][x] == 0)
                        {
                            xs.Add(x);
                            ys.Add(y);
                        }
                    }
                }
                int r = rand.Next(xs.Count);
                arr[ys[r]][xs[r]] = 2;
            }

            Console.SetCursorPosition(0, 0);
            Console.BackgroundColor = ConsoleColor.Black;
            foreach (var item in arr)
            {
                foreach (var item2 in item)
                {
                    string prt = item2 == 0 ? "." : $"{item2}";
                    Console.Write($"{prt}\t");
                    /*Console.BackgroundColor = ConsoleColor.White;
                    Console.Write(" ");
                    Console.BackgroundColor = ConsoleColor.Black;*/
                }
                /*Console.BackgroundColor = ConsoleColor.White;
                Console.WriteLine();
                Console.BackgroundColor = ConsoleColor.Black;*/
                Console.WriteLine();
            }
            Console.WriteLine($"Счёт: {scoup}");
        }
    }
}
